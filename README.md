# cuddly-telegram-newyear2019
## How to run test?
* Install Node.js and npm or yarn
* `git clone https://github.com/fakefred/cuddly-telegram-newyear2019.git`
* `cd ~/cuddly-telegram-newyear2019`
* `npm i` or `yarn`
* `node index.js`
* Open `http://localhost:2019/display` in browser; disable any proxies.
* Open other routes: `/`, `/admin` and `/debug`

> Password is temporarily `hsefz`

Online demo should be available on [http://autometalogolex.me:2019/display](http://autometalogolex.me:2019/display),
    [http://autometalogolex.me:2019/](http://autometalogolex.me:2019/) and other routes defined above.

**Thank you for your patience of testing this application.**

### Development Ongoing: Check
[Projects](https://github.com/fakefred/cuddly-telegram-newyear2019/projects/1?fullscreen=true)
for details.
